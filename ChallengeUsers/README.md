# README #

Check what steps are necessary to get your application up and running.


### What is this repository for? ###

* Challenge project


### What do I need to set up? ###

* PHP (8.0.13)
* MySQL (8.0.27)
* Composer (version 2.1.5)


### Start Application ###

* On folder 'Public', run './start.sh'


### Project Owner ###

* João Mendes Francisco
