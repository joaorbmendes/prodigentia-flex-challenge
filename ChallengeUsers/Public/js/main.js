function emailExists() {

    let inputEmail = document.getElementById('email');
    let email = inputEmail.value;

    const Http = new XMLHttpRequest();
    const url = '/user/emailExists?email=' + email;

    Http.open('GET', url);
    Http.send();

    Http.onreadystatechange = (e) => {

        if (Http.status == 200) {
            let response = JSON.parse(Http.responseText);
            if (response.result) {
                document.getElementById('email').classList.add('is-invalid');
                document.getElementById('emailHelp').innerHTML = 'Email já existe';
            } else {
                document.getElementById('emailHelp').innerHTML = '';
            }
        }
    }

    confirmEmail();
}

function confirmEmail() {

    if (document.getElementById('email').value != document.getElementById('emailConfirm').value) {
        document.getElementById('emailConfirm').classList.add('is-invalid');
        document.getElementById('emailConfirmHelp').innerHTML = 'Email não corresponde';
    } else {
        document.getElementById('emailConfirm').classList.remove('is-invalid');
        document.getElementById('emailConfirmHelp').innerHTML = '';
    }

}

function clearValidation(input) {
    input.classList.remove('is-invalid');
}

function checkPassword() {

    const regexVeryStrong = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/;
    const regexStrong = /^(?=.*[a-z])(?=.*[A-Z]).{8,32}$/;
    const regexWeak = /^(?=.*[a-z]).{6,32}$/;

    let password = document.getElementById('password').value;
    let passwordHelpElement = document.getElementById('passwordHelp');

    removePasswordHelpClass();

    if (password.match(regexVeryStrong)) {

        passwordHelpElement.innerHTML = 'Password Muito Forte';
        passwordHelpElement.classList.add('btn-success');

    } else if (password.match(regexStrong)) {

        passwordHelpElement.innerHTML = 'Password Forte';
        passwordHelpElement.classList.add('btn-info');

    } else if (password.match(regexWeak)) {

        passwordHelpElement.innerHTML = 'Password media fraca';
        passwordHelpElement.classList.add('btn-warning');

    } else {

        passwordHelpElement.innerHTML = 'Password fraca';
        passwordHelpElement.classList.add('btn-danger');
    }

}

function removePasswordHelpClass() {
    document.getElementById('passwordHelp').classList.remove('btn-succsess');
    document.getElementById('passwordHelp').classList.remove('btn-info');
    document.getElementById('passwordHelp').classList.remove('btn-warning');
    document.getElementById('passwordHelp').classList.remove('btn-danger');
}

function confirmPassword() {

    if (document.getElementById('password').value != document.getElementById('passwordConfirm').value) {
        document.getElementById('passwordConfirm').classList.add('is-invalid');
        document.getElementById('passwordConfirmHelp').innerHTML = 'Password não corresponde';
    } else {
        document.getElementById('passwordConfirm').classList.remove('is-invalid');
        document.getElementById('passwordConfirmHelp').innerHTML = '';
    }

}

function validatePhone() {

    const regex = /^9[1236]{1}[0-9]{7}$/;

    let phoneElement = document.getElementById('phone');
    let phoneHelpElement = document.getElementById('phoneHelp');
    let country = document.getElementById('country').value;

    if (country == 'PT' && !phoneElement.value.match(regex)) {
        phone.classList.add('is-invalid');
        phoneHelpElement.innerHTML = 'Número não corresponde a Portugal';
    }
}

function validatePostalcode() {

    const regex = /[0-9]{4}-[0-9]{3}$/;

    let postalcodeElement = document.getElementById('postalcode');
    let postalcodeHelpElement = document.getElementById('postalcodeHelp');

    if (!postalcodeElement.value.match(regex)) {
        postalcodeElement.classList.add('is-invalid');
        postalcodeHelpElement.innerHTML = 'Formato do Código Postal inválido (XXXX-XXX)';
    }
}

function validateNif() {

    const regex = /[0-9]{9}$/;

    let nifElement = document.getElementById('nif');
    let nifHelpElement = document.getElementById('nifHelp');

    if (!nifElement.value.match(regex)) {
        nif.classList.add('is-invalid');
        nifHelpElement.innerHTML = 'Nif tem de ter 9 algarismos numéricos';
    }
}

function validateForm() {
    checkIsEmpty();
    emailExists();
    confirmEmail();
    checkPassword();
    confirmPassword();
    validatePostalcode();
    validateNif();
    validatePhone();

    let invalidInputs = Array.from(document.getElementsByClassName('is-invalid'));

    if (invalidInputs.length > 0) {
        console.log('invalido');
        return false;
    }
    console.log('valido');
}

function clearPhone() {
    document.getElementById('phone').value = '';
}


function checkIsEmpty() {

    let htmlCollection = document.getElementsByClassName('form-control');
    let inputsArray = Array.from(htmlCollection);
    inputsArray.map((input) => input.value === '' ? input.classList.add('is-invalid') : '');
}