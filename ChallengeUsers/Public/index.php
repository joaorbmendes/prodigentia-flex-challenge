<?php
session_start();

require '../config.php';

require '../vendor/autoload.php';


$core = new App\Core\Core();
$core->run();