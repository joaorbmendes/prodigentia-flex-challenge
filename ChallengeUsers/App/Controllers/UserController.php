<?php 

namespace App\Controllers;

use \App\Core\Controller;
use App\Models\User;
use \App\Validators\StoreUserValidator;
use \App\Validators\Validator;


class UserController extends Controller
{

    protected User $user;
    protected StoreUserValidator $storeValidator;


    public function __construct()
    {
        $this->user = new User();
        $this->storeValidator = new StoreUserValidator();
    }


    public function index(array $form = [])
    {

        $_SESSION['csrf'] = Validator::createCsrf();

        if (!isset($form['errors'])) {
            $form['errors'] = [];
        }

        $this->loadView('users/create', $form);
    }

    public function all(bool $created = false)
    {
        $this->loadView('users/list', ['users' => $this->user->all(), 'created' => $created]);
    }

    public function emailExists()
    {
        echo json_encode(['result' => $this->user->emailExists($_GET['email'])]);
    }

    public function create()
    {

        $request = $this->storeValidator->validate($_POST);

        if (!empty($request['errors'])) {
            return $this->index($request);
        }

        $this->user->setProperties(
            firstName: $request['firstName'],
            lastName: $request['lastName'],
            email: $request['email'],
            nif: $request['nif'],
            phone: $request['phone'],
            address: $request['address'],
            postalcode: $request['postalcode'],
            location: $request['location'],
            country: $request['country'],
            password: password_hash($request['password'], PASSWORD_BCRYPT)
        );

        if ($this->user->create()) {
            return $this->all(true);
        } else {
            return $this->index($request);
        }

    }

}