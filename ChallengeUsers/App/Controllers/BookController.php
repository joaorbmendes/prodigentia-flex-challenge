<?php

namespace App\Controllers;


use \App\Core\Controller;
use App\Models\Book;



class BookController extends Controller
{

    protected Book $book;


    public function __construct()
    {
        $this->book = new Book('Books.xml');
    }


    public function index()
    {

        if (
            !empty($_GET['filterMin']) &&
            !empty($_GET['filterMax']) &&
            is_numeric($_GET['filterMin']) &&
            is_numeric($_GET['filterMax'])
        ) {
            $filterMin = $_GET['filterMin'];
            $filterMax = $_GET['filterMax'];
            $books = $this->book->filter($filterMin, $filterMax);
        }

        $response = [
            'books'     => $books ?? $this->book->bookList,
            'minPrice'  => $this->book->minPrice,
            'maxPrice'  => $this->book->maxPrice,
            'filterMin' => $filterMin ?? $this->book->minPrice,
            'filterMax' => $filterMax ?? $this->book->maxPrice,
        ];

        $this->loadView('books/list', $response);
    }
}
