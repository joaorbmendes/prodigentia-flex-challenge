<?php

namespace App\Validators;


Trait Validator
{

    protected function isEmailValid(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    protected function isPhoneValid(string $phone): bool
    {
        $regex = '/^9[1236]{1}[0-9]{7}$/';
        return preg_match($regex, $phone);
    }

    protected function isNifValid(string $nif): bool
    {
        return is_numeric($nif) && strlen($nif) === 9;
    }

    protected function isPostalCodeValid(string $postalCode): bool
    {
        $regex = '/[0-9]{4}-[0-9]{3}$/';
        return preg_match($regex, $postalCode);
    }

    protected function isCountryValid(string $postalCode): bool
    {
        return in_array($postalCode, ['PT', 'ES', 'FR', 'DE']);
    }

    protected function isPasswordValid(string $password): bool
    {
        return strlen($password) >= 4;
    }

    public static function createCsrf(): string
    {
        $options = ['cost' => 8];

        return password_hash(rand(0, 60), PASSWORD_BCRYPT, $options);
    }
}