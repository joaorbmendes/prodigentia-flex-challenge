<?php

namespace App\Validators;


Trait Sanitize
{

    protected function sanitizeString(string $string): string
    {
        return trim(filter_var($string, FILTER_SANITIZE_STRING));
    }

    protected function sanitizeNumber(string $number): int
    {
        return (int) trim(filter_var($number, FILTER_SANITIZE_NUMBER_INT));
    }

    protected function sanitizeEmail(string $email): string
    {
        return trim(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function sanitizeUrl(string $url): string{
        return trim(filter_var($url, FILTER_SANITIZE_URL));
    }
}