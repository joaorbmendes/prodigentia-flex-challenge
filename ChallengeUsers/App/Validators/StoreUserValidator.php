<?php

namespace App\Validators;


class StoreUserValidator
{

    use Sanitize;
    use Validator;



    public function validate(array $inputs): array
    {
        $form['csrf'] = !empty($inputs['csrf']) ? $this->sanitizeString($inputs['csrf']) : '';
        $form['firstName'] = !empty($inputs['firstName']) ? $this->sanitizeString($inputs['firstName']) : '';
        $form['lastName'] = !empty($inputs['lastName']) ? $this->sanitizeString($inputs['lastName']) : '';
        $form['email'] = !empty($inputs['email']) ? $this->sanitizeEmail($inputs['email']) : '';
        $form['nif'] = !empty($inputs['nif']) ? $this->sanitizeNumber($inputs['nif']) : '';
        $form['phone'] = !empty($inputs['phone']) ? $this->sanitizeNumber($inputs['phone']) : '';
        $form['address'] = !empty($inputs['address']) ? $this->sanitizeString($inputs['address']) : '';
        $form['postalcode'] = !empty($inputs['postalcode']) ? $this->sanitizeString($inputs['postalcode']) : '';
        $form['location'] = !empty($inputs['location']) ? $this->sanitizeString($inputs['location']) : '';
        $form['country'] = !empty($inputs['country']) ? $this->sanitizeString($inputs['country']) : '';
        $form['password'] = !empty($inputs['password']) ? $this->sanitizeString($inputs['password']) : '';



        if ($_SESSION['csrf'] !== $form['csrf']) {
            $form['errors'][] = 'invalid token';
            return $form;
        }

        if (empty($form['firstName'])) {
            $form['errors'][] = 'firstName';
        }

        if (empty($form['lastName'])) {
            $form['errors'][] = 'lastName';
        }

        if (!$this->isEmailValid($form['email'])) {
            $form['errors'][] = 'email';
        }

        if (!$this->isNifValid($form['nif'])) {
            $form['errors'][] = 'nif';
        }

        if (!$this->isPhoneValid($form['phone'])) {
            $form['errors'][] = 'phone';
        }

        if (empty($form['address'])) {
            $form['errors'][] = 'address';
        }

        if (!$this->isPostalcodeValid($form['postalcode'])) {
            $form['errors'][] = 'postalcode';
        }

        if (empty($form['location'])) {
            $form['errors'][] = 'location';
        }

        if (!$this->isCountryValid($form['country'])) {
            $form['errors'][] = 'country';
        }

        return $form;
    }


}