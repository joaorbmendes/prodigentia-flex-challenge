<?php 

namespace App\Models;


use App\Core\Model;


class User extends Model {


    public ?int $id;

    public string $firstName;

    public string $lastName;

    public string $email;

    public string $location;

    public string $country;

    protected ?int $nif;

    protected ?string $phone;

    protected ?string $address;

    protected ?string $postalcode;

    private ?string $password;


    
    public function setProperties(
        string $id = null,
        string $firstName,
        string $lastName,
        string $email,
        int $nif = null,
        string $phone = null,
        string $address = null,
        string $postalcode = null,
        string $location,
        string $country,
        string $password = null
    ): void {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->nif = $nif;
        $this->phone = $phone;
        $this->address = $address;
        $this->postalcode = $postalcode;
        $this->location = $location;
        $this->country = $country;
        $this->password = $password;
    }

    public function all()
    {
        $sql = "SELECT `id`, `first_name` as firstName, `last_name` as lastName, `email`, `location`, `country` FROM `users`";
        $sql = $this->db->query($sql);
        return $sql->fetchAll();
    }

    public function emailExists(string $email): bool
    {
        $sql = "SELECT `email` FROM `users` WHERE `email` = :email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        return (bool) $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function create()
    {
        $sql = "INSERT INTO challenge.users
                (`first_name`, `last_name`, `email`, `address`, `phone`, `nif`, `postal_code`, `location`, `country`, `password`)
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $stmt = $this->db->prepare($sql);
        
        return $stmt->execute([
            $this->firstName, 
            $this->lastName,
            $this->email,
            $this->address,
            $this->phone,
            $this->nif,
            $this->postalcode,
            $this->location,
            $this->country,
            $this->password
        ]);

    }

}
