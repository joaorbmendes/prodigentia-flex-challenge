<?php 

namespace App\Models;



class Book {


    public array $bookList;
    public float $minPrice;
    public float $maxPrice;



    public function __construct(string $fileName)
    {
        $this->bookList = $_SESSION['books'] ?? $this->getBooks($fileName);
        $this->minPrice = $_SESSION['minPrice'];
        $this->maxPrice = $_SESSION['maxPrice'];
        $_SESSION['books'] = $this->bookList;
    }

    public function getBooks(string $fileName) : array
    {
        # - Problem with the 'desc' property (<![CDATA[) using the simplexml_load_string() function - #

        $doc = new \DOMDocument();
        $doc->load('../Storage/' . $fileName);
        $items = $doc->getElementsByTagName('item');
        $books = [];

        foreach ($items as $item) {

            foreach($item->childNodes as $child) {

                if ($child->nodeName === 'title') {
                    $title = trim($child->nodeValue);
                }

                if ($child->nodeName === 'author') {
                    $author = trim($child->nodeValue);
                }

                if ($child->nodeName === 'desc') {
                    $desc = trim($child->nodeValue);
                }

                if ($child->nodeName === 'lprice') {
                    $lprice = trim($child->nodeValue);
                }

                if ($child->nodeName === 'cprice') {
                    $cprice = (float) trim($child->nodeValue);

                    if (empty($this->minPrice) || $cprice < $this->minPrice) {
                        $this->minPrice = floor($cprice);
                    }

                    if (empty($this->maxPrice) || $cprice > $this->maxPrice) {
                        $this->maxPrice = ceil($cprice);
                    }
                }
            }

            $books[] = [
                'id'        => $item->getAttribute('id'),
                'title'     => $title,
                'author'    => $author,
                'desc'      => $desc,
                'lprice'    => $lprice,
                'cprice'    => $cprice,
            ];
       
        }

        $_SESSION['minPrice'] = $this->minPrice;
        $_SESSION['maxPrice'] = $this->maxPrice;

        return $books;
    }

    public function filter(float $minPrice, float $maxPrice): array
    {
        return array_filter(
            $this->bookList,
            function ($book) use ($minPrice, $maxPrice) {
                return $book['cprice'] >= $minPrice && $book['cprice'] <= $maxPrice;
            } 
        );
    }
}
