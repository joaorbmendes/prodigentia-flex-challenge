<?php 

namespace App\Core;


use App\Validators\Sanitize;


class Core
{
    use Sanitize;


    public function run()
    {

        $params = [];
  
        if (isset($_SERVER['REQUEST_URI'])) {
            $url = $this->sanitizeUrl($_SERVER['REQUEST_URI']);
        }

        if (!empty($_GET)) {
            $url = (explode('?', $url))[0];
        }

        if (!empty($url) && $url != '/') {

            $url = explode('/', $url);

            array_shift($url);


            $currentController = 'App\\Controllers\\' . ucfirst($url[0]) . 'Controller';


            array_shift($url);

            if (isset($url[0]) && !empty($url[0])) {
                $currentAction = $url[0];
                array_shift($url);
            } else {
                $currentAction = 'index';
            }

            if (count($url) > 0) {
                $params = $url;
            }
        } else {
            $currentController = 'App\Controllers\HomeController';
            $currentAction = 'index';
        }

      
        $c = new $currentController();
        call_user_func_array(array($c, $currentAction), $params);
    }
}