<?php 

namespace App\Core;


class Controller
{
    protected array $request;


    public function loadView(string $viewName, $viewData = array()) :void {
        extract($viewData);
        require '../App/Views/' . $viewName . '.php';
    }

}