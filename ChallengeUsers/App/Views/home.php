<!DOCTYPE html>
<html lang="pt">

<head>
    <title><?= PAGE_TITLE; ?> - Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>

<body>

    <div class="container">

        <section class="py-5 text-center container">
            <div class="row py-lg-5">
                <div class="col-lg-6 col-md-8 mx-auto">
                    <h1 class="fw-light"><?= PAGE_TITLE; ?></h1>
                    <p class="lead text-muted">Desafios PHP Developer</p>
                    <p>
                        <a href="/user" class="btn btn-primary my-2">Users Challenge</a>
                        <a href="/book" class="btn btn-success my-2">Books Challenge</a>
                    </p>
                </div>
            </div>
        </section>
    </div>
</body>

</html>