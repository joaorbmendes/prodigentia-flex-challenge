<!DOCTYPE html>
<html lang="pt">

<head>
  <title><?= PAGE_TITLE; ?> - Livros</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="https://code.jquery.com/resources/demos/style.css">
</head>

<body>

  <div class="container">
    <div class="row mb-3 mt-5">
      <div class="col-md-4">
        <h2>Livros</h2>
      </div>
      <div class="col-md-8 text-right">
        <a href="/" class="btn btn-primary my-2 btn-sm">Home</a>
      </div>
    </div>

    <form action="/book" method="get">
      <p>
        <label for="amount">Preço:</label>
        <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold; width:150px">
      </p>

      <div id="slider-range"></div>
      <input type="hidden" id="filterMin" name="filterMin">
      <input type="hidden" id="filterMax" name="filterMax">
      <button type="submit" class="btn btn-success btn-sm mt-3">Filtrar</button>
    </form>

    <?php if (empty($books)) { ?>
      <p class="lead text-muted text-center">Não existem resultados</p>
    <?php } else { ?>

      <table class="table table-bordered mt-5">
        <thead>
          <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Autor</th>
            <th>Descrição</th>
            <th>lprice</th>
            <th>cprice</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($books as $book) { ?>
            <tr>
              <td><?= $book['id']; ?></td>
              <td><?= $book['title']; ?></td>
              <td><?= $book['author']; ?></td>
              <td><?= $book['desc']; ?></td>
              <td><?= $book['lprice']; ?></td>
              <td><?= $book['cprice']; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    <?php } ?>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
  <script>
    $(function() {
      $("#slider-range").slider({
        range: true,
        min: <?= $minPrice ?>,
        max: <?= $maxPrice ?>,
        values: [<?= $filterMin ?>, <?= $filterMax ?>],
        slide: function(event, ui) {
          $("#amount").val("€" + ui.values[0] + " - €" + ui.values[1]);
          $("#filterMin").val(ui.values[0]);
          $("#filterMax").val(ui.values[1]);
        }
      });
      $("#amount").val("€" + $("#slider-range").slider("values", 0) +
        " - €" + $("#slider-range").slider("values", 1));
    });
  </script>
</body>

</html>