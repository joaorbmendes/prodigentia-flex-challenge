<!DOCTYPE html>
<html lang="en">

<head>
  <title><?= PAGE_TITLE; ?> - Utilizadores</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>

<body>

  <div class="container">

    <div class="row mb-3 mt-5">
      <div class="col-md-4">
        <h2>Utilizadores</h2>
      </div>
      <div class="col-md-8 text-right">
        <a href="/user" class="btn btn-primary my-2 btn-sm">Criar novo</a>
        <a href="/" class="btn btn-primary my-2 btn-sm">Home</a>
      </div>
    </div>

    <?php if ($created) { ?>
      <div class="alert alert-success" role="alert">
        Utilizador criado com sucesso!
      </div>
    <?php } ?>


    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Id</th>
          <th>Nome</th>
          <th>Apelido</th>
          <th>Email</th>
          <th>Localidade</th>
          <th>País</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($users as $user) { ?>
          <tr>
            <td><?= $user['id']; ?></td>
            <td><?= $user['firstName']; ?></td>
            <td><?= $user['lastName']; ?></td>
            <td><?= $user['email']; ?></td>
            <td><?= $user['location']; ?></td>
            <td><?php
                echo match ($user['country']) {
                  'ES' => 'Espanha',
                  'FR' => 'França',
                  'DE' => 'Alemanha',
                  default => 'Portugal',
                };
                ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

</body>

</html>