<!doctype html>
<html lang="pt">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title><?= PAGE_TITLE; ?> - Criar utilizador</title>
</head>

<body>

    <div class="container">

        <div class="row mb-3 mt-5">
            <div class="col-md-4">
                <h2>Criar utilizador</h2>
            </div>
            <div class="col-md-8 text-right">
                <a href="/user/all" class="btn btn-primary my-2 btn-sm">Listar</a>
                <a href="/" class="btn btn-primary my-2 btn-sm">Home</a>
            </div>
        </div>
        

        <?php if (!empty($errors)) { ?>
            <div class="alert alert-danger" role="alert">
                Existem erros no formulário
            </div>
        <?php } ?>


        <form action="/user/create" onsubmit="return validateForm()" method="post">
            <div class="row mb-3">
                <div class="col-md-4">
                    <label for="email" class="form-label">Email:</label>
                    <input type="email" onblur="emailExists()" onfocus="clearValidation(this)" class="form-control <?php in_array('email', $errors) ? print('is-invalid') : '' ?>" id="email" name="email" aria-describedby="emailHelp" value="<?= $email ?? '' ?>" required>
                    <div id="emailHelp" class="form-text"></div>
                </div>
                <div class="col-md-4">
                    <label for="emailConfirm" class="form-label">Confirmar Email:</label>
                    <input type="email" onblur="confirmEmail()" onfocus="clearValidation(this)" class="form-control" id="emailConfirm" aria-describedby="emailConfirmHelp" required>
                    <div id="emailConfirmHelp" class="form-text"></div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-4">
                    <label for="password" class="form-label">Password:</label>
                    <input type="password" onkeyup="checkPassword()" onfocus="clearValidation(this)" class="form-control <?php in_array('password', $errors) ? print('is-invalid') : '' ?>" id="password" aria-describedby="passwordHelp" required>
                    <div id="passwordHelp" class="form-text p-1"></div>
                </div>
                <div class="col-md-4">
                    <label for="passwordConfirm" class="form-label">Confirmar Password:</label>
                    <input type="password" onblur="confirmPassword()" onfocus="clearValidation(this)" class="form-control" id="passwordConfirm" aria-describedby="passwordConfirmHelp" required>
                    <div id="passwordConfirmHelp" class="form-text"></div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-4">
                    <label for="firstName" class="form-label">Nome:</label>
                    <input type="text" onfocus="clearValidation(this)" class="form-control <?php in_array('firstName', $errors) ? print('is-invalid') : '' ?>" id="firstName" name="firstName" value="<?= $firstName ?? '' ?>" required>
                </div>
                <div class="col-md-4">
                    <label for="lastName" class="form-label">Apelido:</label>
                    <input type="text" onfocus="clearValidation(this)" class="form-control <?php in_array('lastName', $errors) ? print('is-invalid') : '' ?>" id="lastName" name="lastName" value="<?= $lastName ?? '' ?>" required>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-8">
                    <label for="address" class="form-label">Rua / Nº:</label>
                    <input type="text" onfocus="clearValidation(this)" class="form-control <?php in_array('address', $errors) ? print('is-invalid') : '' ?>" id="address" name="address" value="<?= $address ?? '' ?>" required>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-4">
                    <label for="postalcode" class="form-label">Codigo Postal:</label>
                    <input type="text" onblur="validatePostalcode()" onfocus="clearValidation(this)" class="form-control <?php in_array('postalcode', $errors) ? print('is-invalid') : '' ?>" id="postalcode" name="postalcode" value="<?= $postalcode ?? '' ?>" maxlength="8" required>
                    <div id="postalcodeHelp" class="form-text"></div>
                </div>
                <div class="col-md-4">
                    <label for="location" class="form-label">Localidade:</label>
                    <input type="text" onfocus="clearValidation(this)" class="form-control <?php in_array('location', $errors) ? print('is-invalid') : '' ?>" id="location" name="location" value="<?= $location ?? '' ?>" required>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-8">
                    <label for="country" class="form-label">País:</label>
                    <select class="form-select form-select-md <?php in_array('country', $errors) ? print('is-invalid') : '' ?>" id="country" name="country" onfocus="clearValidation(this)" onchange="clearPhone()" required>
                        <option <?= empty($country) ? 'selected' : '' ?>>...</option>
                        <option <?= !empty($country) && $country === 'PT' ? 'selected' : '' ?> value="PT">Portugal</option>
                        <option <?= !empty($country) && $country === 'ES' ? 'selected' : '' ?> value="ES">Espanha</option>
                        <option <?= !empty($country) && $country === 'FR' ? 'selected' : '' ?> value="FR">França</option>
                        <option <?= !empty($country) && $country === 'DE' ? 'selected' : '' ?> value="DE">Alemanha</option>
                    </select>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-md-4">
                    <label for="nif" class="form-label">NIF:</label>
                    <input type="text" onblur="validateNif()" onfocus="clearValidation(this)" class="form-control <?php in_array('nif', $errors) ? print('is-invalid') : '' ?>" id="nif" name="nif" value="<?= $nif ?? '' ?>" maxlength="9" required>
                    <div id="nifHelp" class="form-text"></div>
                </div>
                <div class="col-md-4">
                    <label for="phone" class="form-label">Telemovel:</label>
                    <input type="text" onblur="validatePhone()" onfocus="clearValidation(this)" class="form-control <?php in_array('phone', $errors) ? print('is-invalid') : '' ?>" id="phone" name="phone" value="<?= $phone ?? '' ?>" required>
                    <div id="phoneHelp" class="form-text"></div>
                </div>
            </div>

            <input type="hidden" name="csrf" value="<?= $_SESSION['csrf'] ?? '' ?>">


            <button type="submit" class="btn btn-success">Guardar</button>
        </form>

    </div>
    <script src="<?= BASE_URL ?>js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>