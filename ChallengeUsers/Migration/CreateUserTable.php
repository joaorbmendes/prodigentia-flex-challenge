<?php

$sql = "CREATE TABLE IF NOT EXISTS `users` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100) NOT NULL,
    `last_name` varchar(100) NOT NULL,
    `email` varchar(100) NOT NULL,
    `address` varchar(250) NOT NULL,
    `phone` varchar(20) NOT NULL,
    `nif` int unsigned NOT NULL,
    `postal_code` varchar(8) NOT NULL,
    `location` varchar(50) NOT NULL,
    `country` enum('PT','EN','FR','ES') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    `password` varchar(100) NOT NULL,
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime,
    PRIMARY KEY (`id`),
    UNIQUE KEY `users_email_IDX` (`email`) USING BTREE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
$db->exec($sql);
