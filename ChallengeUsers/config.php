<?php
require 'environment.php';

$config = [];

define('PAGE_TITLE', 'Prodigentia Challenge');

global $db;


if (ENVIRONMENT == 'development') {
    
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    define('BASE_URL', 'http://localhost:8000/');
    $config['dbname'] = 'challenge';
    $config['host']   = 'localhost';
    $config['dbuser'] = 'root';
    $config['dbpass'] = 'root';

} else {
    //define('BASE_URL', 'http://localhost:8000/');
    //$config['dbname'] = 'challenge';
    //$config['host']   = 'mysql';
    //$config['dbuser'] = 'root';
    //$config['dbpass'] = 'root';
}


try {
    $db = new PDO('mysql:dbname='.$config['dbname'].';host='.$config['host'], $config['dbuser'], $config['dbpass']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    require 'Migration/CreateUserTable.php';

} catch(PDOException $e) {
    echo 'ERRO: ' . $e->getMessage();
    exit;
}
